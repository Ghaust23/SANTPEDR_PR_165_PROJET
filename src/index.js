const mongoose = require('mongoose')
const express = require('express')
const app = express()
const port = 3000

app.use(express.json())

const cours = require('./routes/cours')
const ecole = require('./routes/ecole')
const proprietaire = require('./routes/proprietaire')
const session = require('./routes/session')

 
main().catch(err => console.log(err))

async function main() {
  await mongoose.connect('mongodb://127.0.0.1:27017/coursCanin')
  console.log("Connected to the database")

  app.get('/', async (req, res) => {
    res.send("Welcome the the REST API")
  })

  app.use('/cours', cours)
  app.use('/ecole', ecole)
  app.use('/session', session)
  app.use('/proprietaire', proprietaire)

  await app.listen(port)
  console.log(`App listening on port ${port}`)
}