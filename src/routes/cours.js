const mongoose = require('mongoose')
const express = require('express')
const router = express.Router()

const coursModel = require('../models/coursModel')

router.get('/', async (req, res) => {
    console.log(req.body)
    cours = await coursModel.find()
    res.send(cours)
})

router.post('/', (req, res) => {
    console.log(req.body)
    coursModel.insertMany(req.body)
    res.send(`${req.body}`)
})

router.put('/:id', async (req, res) => {
    console.log(req.body)
    
    const update = await coursModel.updateMany({"_id": req.params.id}, req.body)
    res.send(update)
})

router.delete(`/:id`, async (req, res) => {
    console.log(req.body)
    deleted = await coursModel.findOneAndDelete({"_id" : req.params.id})

    res.send(deleted)
})


module.exports = router