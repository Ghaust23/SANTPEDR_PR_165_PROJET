const mongoose = require('mongoose')
const express = require('express')
const router = express.Router()

const proprietaireModel = require('../models/proprietaireModel')

router.get('/', async (req, res) => {
    proprietaire = await proprietaireModel.find()
    res.send(proprietaire)
})

router.post('/', (req, res) => {
    console.log(req.body)
    proprietaireModel.insertMany(req.body)
    res.send(`${req.body}`)
})

router.put('/:id', async (req, res) => {
    console.log(req.body)
    
    const update = await proprietaireModel.updateMany({"_id": req.params.id}, req.body)
    res.send(update)
})

router.delete(`/:id`, async (req, res) => {
    console.log(req.body)
    deleted = await proprietaireModel.findOneAndDelete({"_id" : req.params.id})

    res.send(deleted)
})

module.exports = router