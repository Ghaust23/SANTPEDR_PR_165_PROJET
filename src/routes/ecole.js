const mongoose = require('mongoose')
const express = require('express')
const router = express.Router()

const ecoleModel = require('../models/ecoleModel')

router.get('/', async (req, res) => {

    ecole = await ecoleModel.find()
    res.send(ecole)
})

router.post('/', (req, res) => {
    console.log(req.body)
    ecoleModel.insertMany(req.body)
    res.send(`${req.body}`)
})

router.put('/:id', async (req, res) => {
    console.log(req.body)
    
    const update = await ecoleModel.updateMany({"_id": req.params.id}, req.body)
    res.send(update)
})

router.delete(`/:id`, async (req, res) => {
    console.log(req.body)
    deleted = await ecoleModel.findOneAndDelete({"_id" : req.params.id})

    res.send(deleted)
})

module.exports = router