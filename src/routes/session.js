const mongoose = require('mongoose')
const express = require('express')
const router = express.Router()

const sessionModel = require('../models/sessionModel')
// Note à soi-même: utiliser Edge pour afficher des trucs .json

// Montre toutes les sessions avec des participations disctinctes (elles étaient par listes, on fait une instance par élément de liste)
router.get('/unwind', async (req, res) => {
    session = await sessionModel.aggregate([
        {
          $unwind: {
            path: "$participations"
          }
        }
      ])
    res.send(session)
})

// Montre toutes les valeurs distinctes de dureeM
router.get('/group', async (req, res) => {
  session = await sessionModel.aggregate([
      {
        $group : { 
            _id : "$dureeM"
        }
      }
    ])
  res.send(session)
})

// Montre les id de cours dans la collection session en tant que les_cours

router.get('/lookup', async (req, res) => {
    session = await sessionModel.aggregate([
        {
          $lookup: {
            from: "cours",
            localField: "cours_id",
            foreignField: "_id",
            as: "les_cours"
          }
        }
    ])
    res.send(session)
})

// Montre toutes les sessions, mais seulement les attributs cours_id et places
router.get('/project', async (req, res) => {
    session = await sessionModel.aggregate([
        {
          $project: {
            "cours_id" : 1, 
            "places" : 1
          }
        }
    ])
    res.send(session)
})

// Montre toutes les dureeM plus grandes que 180
router.get('/match', async (req, res) => {
    session = await sessionModel.aggregate([
        {
            $match: {
                "dureeM" : {$gt : 180}
            }
        }
    ])
    res.send(session)
})

router.get('/', async (req, res) => {
    session = await sessionModel.find()
    res.send(session)
})

router.post('/', (req, res) => {
    console.log(req.body)
    sessionModel.insertMany(req.body)
    res.send(`${req.body}`)
})

router.put('/:id', async (req, res) => {
  console.log(req.body)
  
  const update = await sessionModel.updateMany({"_id": req.params.id}, req.body)
  res.send(update)
})

router.delete(`/:id`, async (req, res) => {
  console.log(req.body)
  deleted = await sessionModel.findOneAndDelete({"_id" : req.params.id})

  res.send(deleted)
})

module.exports = router