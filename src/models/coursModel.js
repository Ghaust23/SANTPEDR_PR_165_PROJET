const mongoose = require('mongoose')

const Schema = mongoose.Schema;
const model = mongoose.model;

const coursSchema = new Schema({
    "_id": mongoose.ObjectId,
    "tarif": Number,
    "niveau": String,
    "type": String,
}, { collection: "cours" })

module.exports = model("cours", coursSchema);