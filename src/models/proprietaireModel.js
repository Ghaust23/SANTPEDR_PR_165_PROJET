const mongoose = require('mongoose')

const Schema = mongoose.Schema;
const model = mongoose.model;

const proprietaireSchema = new Schema({
    "_id": mongoose.ObjectId,
    "adresse": String,
    "certificatDelivre": Date,
    "dateNaissance": Date,
    "nom": String,
    "origine": String,
    "prenom": String,
    "chiens": [
        {
            "nom": String,
            "male": Boolean,
            "identificationAmicus": String,
            "race": String,
            "_id": mongoose.ObjectId
        }
    ],
    "localite": String,
    "npa": Number,
    "mails": [
        {
            "mail": String,
            "type" : {type: String}
        }
    ],
    "telephones": [
        {
            "numero": String,
            "type": {type: String}
        }
    ],
}, { collection: "proprietaire" })

module.exports = model("proprietaire", proprietaireSchema);