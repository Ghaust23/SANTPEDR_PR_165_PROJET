const mongoose = require('mongoose')

const Schema = mongoose.Schema;
const model = mongoose.model;

const sessionSchema = new Schema({
    "_id": mongoose.ObjectId,
    "date": Date,
    "dureeM": Number,
    "moniteurId": String,
    "places": Number,
    "participations": [
        {
            "chienId": String,
            "avecProprietaire": Boolean
        }
    ],
    "cours_id": mongoose.ObjectId,
}, { collection: "session" })

module.exports = model("session", sessionSchema);